# Summary

* [Introduction](README.md)

* [Chapter 01: Getting Started](chapter-01-getting-started/README.md)

* [Chapter 02: Hello World Extension](chapter-02-hello-world-extension/README.md)

* [Chapter 03: Budget Manager](chapter-03-budget-manager/README.md)

* [Chapter 04: Page Action Extension](chapter-04-page-action-extension/README.md)

* [Chapter 05: Wikit Extension](chapter-05-wikit-extension/README.md)

* [Chapter 6: Speak-It Extension](chapter-06-speak-it-extension/README.md)

* [Chapter 7: Debug and Deploy Chrome Extensions](chapter-07-debug-and-deploy-chrome-extensions/README.md)
