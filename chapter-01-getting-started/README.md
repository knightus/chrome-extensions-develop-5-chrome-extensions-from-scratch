# Chapter 01: Getting Started

## Course Overview
- **Instructor:** [Vishwas Gopinath](https://www.udemy.com/user/vishwas-gopinath-2/)

### What Chrome extensions are
- Small programs that add functionality to Chrome browser
- Can even modify the appearance of a webpage

### What need to know before get started
Basic knowledge of:
- HTML
- CSS
- Javascript
- jQuery (optional)

### Course structure
We gonna build 5 extensions:
- Hello World Extension
- Browser Action Extension
- Page Action Extension
- Neither BA or PA

And how to debug & deloy them to Chrome Web Store

## The Big Picture
How Chrome Extension works:
A Web Extension includes:
- Manifest file
- HTML files
- CSS
- Javascript

packed into a .crx zipped file.

This zipped file will be deployed to Chrome Web Store where users can install the extension
