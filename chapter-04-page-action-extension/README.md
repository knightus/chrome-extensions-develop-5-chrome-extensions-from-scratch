# Chapter 4: Page Action Extension

## Overview
- Works on specific pages
- Inactive & greyed out in other pages

We gonna build a **PageFontStyle** extension:
- Allow us to change font style
- On `developer.chrome.com/*` only
- Basic styling will be applied

## Manifest file
Similar to Budget Manager's manifest file, but replace `browser_action` with `page_action` instead.

## Popup HTML

## Event Page

## Content Scripts and Messages
- Run in the context of web page
- Can change DOM (font color, hyperlinks, rearrange DOM nodes, ...)
- Limitation: Cannot use most of Chrome API
- Solution: Exchange messages among content, background & popup scripts

![screenshot-001](screenshot-001.png)

### Environment of Content scripts
- Isolated
- Accessible to DOM but not variables & functions

## content.js
```
chrome.runtime.sendMessage({})
```
```
chome.runtime.onMessage.addListener(function(request, sender, sendResponse){
   ...
})
```

## content.css

## Color picker
- Get from jscolor.com

## popup.js

## Page Action Extension Summary
- Runs on specific pages
- Content scripts
- Messages
- JS & CSS Injection
