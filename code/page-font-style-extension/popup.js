$(function () {
    let color = $('#font_color').val();
    $('#font_color')
        .on('change paste keyup', function () {
            color = $(this).val();
        })
    ;

    $('#button_submit').on('click', function () {
        chrome.tabs.query({
            active: true,
            currentWindow: true,
        }, function (tabs) {
            chrome.tabs.sendMessage(tabs[0].id, { todo: 'changeColor', clickedColor: color });
        })
    });
});
