let menuItem = {
    id: 'speakIt',
    title: 'Speak',
    contexts: [
        'selection',
    ]
};

chrome.contextMenus.create(menuItem);

chrome.contextMenus.onClicked.addListener(function (event) {
    if (event.menuItemId === 'speakIt' && event.selectionText) {
        chrome.tts.speak(event.selectionText, {
            rate: 1.5
        })
    }
});
