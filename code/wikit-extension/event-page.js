let menuItem = {
    id: 'wikit',
    title: 'Wikit',
    contexts: [
        'selection',
    ]
};

chrome.contextMenus.create(menuItem);

function fixEncodeURI(string) {
    return encodeURI(string).replace(/%5B/g, '[').replace(/%5D/g, ']');
};

chrome.contextMenus.onClicked.addListener(function (event) {
    if (event.menuItemId === 'wikit' && event.selectionText) {
        let wikiUrl = 'https://en.wikipedia.org/wiki/' + fixEncodeURI(event.selectionText);
        let createData = {
            url: wikiUrl,
            type: 'popup',
            top: 5,
            left: 5,
            width: screen.availWidth / 2,
            height: screen.availHeight / 2
        }
        chrome.windows.create(createData, function () {});
    }
});
