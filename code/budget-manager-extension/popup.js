$(function () {
    chrome.storage.sync.get(['total', 'limit'], function(budget) {
        $('#total').text(budget.total);
        $('#limit').text(budget.limit);
    });

    $('#spend_amount').on('click', function () {
        chrome.storage.sync.get(['total', 'limit'], function (budget) {
            console.log('limit', budget.limit);
            let newTotal = 0;
            if (budget.total) {
                newTotal += parseInt(budget.total);
            }

            let amount = $('#amount').val();
            if (amount) {
                newTotal += parseInt(amount);
            }

            chrome.storage.sync.set({ total: newTotal }, function(){
                if (amount && newTotal >= budget.limit) {
                    let notifyOptions = {
                        type: 'basic',
                        iconUrl: 'icon-48.png',
                        title: 'Limit reached.',
                        message: 'Uh oh! Looks like you\'ve reached your limit!'
                    };

                    chrome.notifications.create('limitNotification', notifyOptions);
                }
            });

            $('#total').text(newTotal);
            $('#amount').val('');
        });
    });
});
