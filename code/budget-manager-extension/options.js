$(function () {
    chrome.storage.sync.get('limit', function(budget){
        $('#limit').val(budget.limit);
    });

    $('#save_limit').on('click', function () {
        let limit = $('#limit').val();
        if (limit) {
            chrome.storage.sync.set({ limit: limit }, function () {
                close();
            });
        }
    });

    $('#reset_total').on('click', function () {
        chrome.storage.sync.set({ total: 0 }, function(){
            let notifyOptions = {
                type: 'basic',
                iconUrl: 'icon-48.png',
                title: 'Total reset!',
                message: 'Total has been reset to 0!'
            };

            chrome.notifications.create('limitNotification', notifyOptions);
        });
    });
});
