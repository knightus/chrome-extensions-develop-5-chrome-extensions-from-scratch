let contextMenuItem = {
    id: "spendMoney",
    title: "SpendMoney",
    contexts: ["selection"]
};

chrome.contextMenus.create(contextMenuItem);

function isInt(value) {
    return !isNaN(value) && parseInt(Number(value)) == value && !isNaN(parseInt(value, 10));
}

chrome.contextMenus.onClicked.addListener(function (event) {
    if (event.menuItemId === "spendMoney" && event.selectionText) {
        if (isInt(event.selectionText)) {
            chrome.storage.sync.get(['total', 'limit'], function (budget) {
                let newTotal = 0;
                if (budget.total) {
                    newTotal += parseInt(budget.total);
                }
                newTotal += parseInt(event.selectionText);
                chrome.storage.sync.set({ total: newTotal }, function () {
                    if (newTotal >= budget.limit) {
                        let notifyOptions = {
                            type: 'basic',
                            iconUrl: 'icon-48.png',
                            title: 'Limit reached.',
                            message: 'Uh oh! Looks like you\'ve reached your limit!'
                        };

                        chrome.notifications.create('limitNotification', notifyOptions);
                    }
                });
            })
        }
    }
});

chrome.storage.onChanged.addListener(function (event, storageName) {
    chrome.browserAction.setBadgeText({ text: event.total.newValue.toString() });
});
