# Chrome Extensions : Develop 5 chrome extensions from scratch - Aug 2018

Journal for Learning Course: Chrome Extensions : Develop 5 chrome extensions from scratch https://www.udemy.com/chrome-extension-dev/

## Planning
- Expected Time: 15 hours
- Finish day: 16/08/2018

## Certificate
![certificate](UC-821AM6QD.jpg)

## Day 1 (13/08/2018)
### Today's progress
Learned about the extension definition & course structure and what extensions we gonna build

### Thoughts
That might be easier since I have experienced with Web Development

### Links to work
[Chapter 1](chapter-01-getting-started/README.md)

## Day 2 (14/08/2018)
### Today's progress
Learned about the extension types & structure of manifest file

### Links to work
- [Extension types](chapter-02-hello-world-extension/README.md#extension-types)
- [Manifest File](chapter-02-hello-world-extension/README.md#manifest-file)

## Day 3 (15/08/2018)
### Today's progress
Learned about how to initialize Hello world Extension & apply basic style, script, asset files

### Links to work
- [Chapter 2: Hello World Extension](chapter-02-hello-world-extension/README.md#html-file)

## Day 4 (25/08/2018)
### Today's progress
- Hello World Extension Summary
- Learned about first permission (`storage`)
- Built the popup interface for Budget Manager extension

### Links to work
- [Hello World Extension Summary](chapter-02-hello-world-extension/README.md#hello-world-extension-summary)
- [Chapter 03](chapter-03-budget-manager/README.md)

## Day 5 (26/08/2018)
### Today's progress
- Added interactions to Budget Manager extension
- Learned how to create Options page

### Links to work
- [popup.js](chapter-03-budget-manager/README.md#popup.js)
- [Options page](chapter-03-budget-manager/README.md#options-page)

## Day 6 (27/08/2018)
### Today's progress
- Rich notifications
- Background & event page definitions
- Learned how to create a context menu

### Links to work
- [Rich Notifications](chapter-03-budget-manager/README.md#rich-notifications)
- [Background & event page](chapter-03-budget-manager/README.md#background-&-event-pages)
- [Context menus](chapter-03-budget-manager/README.md#context-menus)

## Day 7 (28/08/2018)
### Today's progress
- Badges
- Page Action Extension

### Links to work
- [Bages](chapter-03-budget-manager/README.md#badges)
- [Chapter 4: Page Action Extension](chapter-04-page-action-extension/README.md)

## Day 8 (30/08/2018)
### Today's progress
- Chapter 5: Wikit Extension
- Chapter 6: Speak-It Extension
- Chapter 7: Debug and Deploy Chrome Extensions

### My thoughts
The deploy to Chrome Web Store process seems to be outdated 

### Links to work
- [Chapter 5](chapter-05-wikit-extension/README.md)
- [Chapter 6](chapter-06-speak-it-extension/README.md)
- [Chapter 7](chapter-07-debug-and-deploy-chrome-extensions/README.md)
