# Chapter 2: Hello World Extension

## Overview

### Extension types
3 types:

#### Browser Action
- Stay in toolbar
- Accessible at all times

#### Page Action
- Accessible only on certain pages
- Stay in toolbar but greyed out

#### Other (Neither BA or PA)
- Run in the background

Hello World Extention will gonna be a **Browser Extension**

## Manifest File
- Store information about the extension
- JSON format
- Mandatory fields:
  - `manifest_version`: Manifest version
  - `name`: Name of the extension
  - `version`: Version of the extension
- Other fields:
  - `description`
  - `icons`: An object which will specify the paths to icons which will be shown in certain places of the browser. Three sizes:
    - `128`: 128 x 128
    - `48`: 48 x 48
    - `16`: 16 x 16
  - `browser_action`
    - `default_icon`
    - `default_popup`: Must be path to a HTML file

## HTML File
The extension can have multiple HTML files to represent the UIs

### popup.html file
Typical HTML file

### How to load unpacked extension to Chrome
- Go to `chrome://extensions`
- Enable `Developer mode`
- Click `Load unpacked` (or `Load unpacked extension...` in older version)
- Choose the extension folder

Result:
![Result](screenshot-001.png)

![Result](screenshot-002.png)

## Javascript File

## Adding jQuery

## Adding CSS

## Hello World Extension summary
- Manifest
- Icons
- HTML file
- JS file
- CSS
- Adding extension onto Chrome
