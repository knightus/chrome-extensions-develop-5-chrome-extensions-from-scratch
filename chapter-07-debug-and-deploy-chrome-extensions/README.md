# Chapter 7: Debug and Deploy Chrome Extensions

## Debug Popup and Options
- Right click on extension icon, then choose **Inspect Popup**
- Debugging will be the same as any normal web page

## Debug Background Pages and Content Scripts
### Debugging Background pages
Will be slightly different:
- Go to Extensions page
- Click on _background page_ of Inspect views of corresponding extension

### Content Scripts
- Open the **Sources** tab of Developer tool
- Select tab **Content scripts**

### Deploy Chrome Extensions
- Seach for `chrome dashboard`
- 
