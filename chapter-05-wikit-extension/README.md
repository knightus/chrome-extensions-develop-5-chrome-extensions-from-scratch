# Chapter 5: Wikit Extension

## Overview
We gonna create an extension which we can select a word & directly search on Wikipedia for that word

## Manifest file

## Event Page
```
chrome.windows.create({
    url: wikiUrl,
    type: 'popup',
    top: 5,
    left: 5,
    width: screen.availWidth / 2,
    height: screen.availHeight / 2
}, function () {});
```
