# Chapter 3: Budget Manager

## Overview
- It's a browser action extension
- Accept spending
- Display total spending
- There's a limit & notify if reached
- There're option to reset total & limit

## Manifest file
Similar to Hello World Extension's manifest file

## Popup HTML File
[popup.html](./../code/budget-manager-extension/popup.html)

## Adding Javascript
- When we enter amount & click Spend button, the total value needs to be updated

### Chrome Storage
- Can be used to get/set the values
- In order to use Chrome Storage, we need to add `storage` to `permissions` array of `manifest.json` file

## popup.js
### Scenarios
- `Total === 0`:
`newTotal = 0 + enteredAmount`

- `Total > 0`:
`newTotal = total + enteredAmount`

### Chrome Storage
Using `get` & `set` method of `chrome.storage.sync` to retrieve & assign value to Chrome Storage:
#### .get()
```
chrome.storage.sync.get('total', function (budget) {
    ...
}
```
#### .set()
```
chrome.storage.sync.set({ total: newTotal });
```

## Options Page
- Can be viewed by right clicking on the entension icon
- Similar to popup page
- Allow to:
  - Set a limit
  - Reset total

## Options HTML

## Options Javascript

## Rich Notifications
- We will use notifications to inform user when the budget is over-limit
- Need `notifications` permission

## Background & Event pages
When we need some code to run on background instead.

- **Background page:** Runs in the background at all time
- An **event page** only runs when it is required

Since background page consumes resources even when it is not required. So it's best to avoid using them & stick to using event pages as much as possible.

We gonna use background & event pages to create a **context menu**

## Context Menus
A context menu is a menu that appears on some user interactions like clicking the right mouse button.

## Creating context menu
- Context menu needs to run in the background
- Only displays when we select some numbers on the web page
Therefore, what we need is an event page, not a background one

Firstly, we need to specify it on the manifest file

## Context Menu Functionality
```
chrome.contextMenus.onClicked.addListener(function (event) {
    ...
}
```

## Badges
```
chrome.browserAction.setBadgeText({})
```

## Budget Manager Summary
